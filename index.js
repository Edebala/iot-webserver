const express = require('express');
const onoff = require('onoff');
const mqtt = require('mqtt');
const MongoClient = require('mongodb').MongoClient;

const app = express();

const webpageHost = '127.0.0.1';
const webpagePort = '3000';

const mqttURL = 'mqtt://192.168.1.3:1883';

const mongoURL = 'mongodb://127.0.0.1:27017';

const clientId = 'mqttjs_' + Math.random().toString(8).substr(2, 4) 
const client  = mqtt.connect(mqttURL, {clientId: clientId}); 


MongoClient.connect(mongoURL, function(err, db) {
				  if (err) throw err;
				  console.log("Database created!");
				  db.close();
});


client.on('connect', function(connack) { 
	console.log("connected to mqtt broker");
	// can also accept objects in the form {'topic': qos} 
	client.subscribe('test/message', function(err, granted){ 
		if(err) { 
			console.log(err, 'err'); 
		} 
     console.log(granted, 'granted');
	}) 
});

client.on('message', (topic, payload) => {
	console.log('Received Message:', topic, payload.toString())
	MongoClient.connect(mongoURL, function(err, db) {
	if (err) throw err;
	var dbo = db.db("mydb");
	var myobj = { value: payload.toString(), timestamp:Date() };
	dbo.collection("Datapoints").insertOne(myobj, function(err, res) {
		  if (err) throw err;
		  console.log("1 document inserted");
		  db.close();
		});
	}); 
})
/*
client.end(false, {}, () => {
	console.log('client disconnected')
})
*/

app.get('/', (req, res) => {
	MongoClient.connect(mongoURL, function(err, db) {
		if (err) throw err;
		var dbo = db.db("mydb");
		dbo.collection("Datapoints").findOne({}, function(err, result) {
			if (err) throw err;
			console.log(result.value);
			db.close();
			res.send(result.value);
		});
	}); 
});

app.listen(webpagePort,()=>{
	console.log('express fut a ' + webpagePort + '-es porton');
})
